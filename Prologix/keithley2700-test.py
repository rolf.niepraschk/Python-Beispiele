#!/usr/bin/env python3

# Rolf Niepraschk, Rolf.Niepraschk@ptb.de, 2022-11-29

import socket, signal, sys, re, time

PROLOGIX_IP = '172.30.54.71'
PROLOGIX_PORT = 1234
SOCKET_TIMEOUT = 10 # s
DEVICE_TIMEOUT = 3000 # ms (max. 3000)
KEITHLEY2700_ADR = 6
TX_EOL = b'\n' # Endemarkierung beim Senden

BUFSIZE = 256

sock = None

def init_socket():
    try:    
        x = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
        x.settimeout(SOCKET_TIMEOUT)
        x.connect((PROLOGIX_IP, PROLOGIX_PORT))
        time.sleep(.1)
    except socket.error as e:
        print ('Error creating socket: %s' % e)  
        sys.exit(1) 
    return x
    
def socket_read():
    return sock.recv(BUFSIZE).decode('ascii').strip()
    
def gpib_write(s):
    try:
        return sock.send(s.encode('ascii') + TX_EOL)
    except socket.error as e:
        print ('Error write to socket: %s' % e)
        return 0
    
def gpib_read():
    try:
        gpib_write('++read eoi')
        return socket_read()
    except socket.error as e:
        print ('Error read from socket: %s' % e)
        return ''

current_device_address = 0
def set_gpib_addr(n):
    global current_device_address
    if n != current_device_address:
        gpib_write('++addr ' + str(n))
        current_device_address = n
    
def init_prologix():
    gpib_write('++savecfg 0')# Don't write configuration to eprom (!)
    gpib_write('++mode 1')# Set mode as controller
    #gpib_write('++ifc')# Controller-In-Charge # nötig?
    gpib_write('++auto 0')# Turn off read-after-write
    gpib_write('++ver')
    print('='*72)
    print(socket_read())
    print('='*72)
    gpib_write('++read_tmo_ms ' + str(DEVICE_TIMEOUT))
    
def init_keithley2700(adr):
    set_gpib_addr(adr)
    gpib_write('++clr')
    gpib_write('*IDN?')
    print(gpib_read())
    print('='*72)
    gpib_write('INIT:CONT OFF')# Continuous measurement mode off
    gpib_write('TRIG:COUN 1')
    gpib_write('TRIG:SOUR IMM')
    # Messgröße, Messbereich und Auflösung anpassen
    gpib_write('CONF:VOLT:DC')
    time.sleep(.5)# nötig?

extract_pattern = re.compile(r'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?')        
def extract_value(d):
    '''
    -3.43784839E-02VDC,+727.217SECS,+00148RDNG#
    '''
    try:
        a = d.split(',')
        x = extract_pattern.search(a[0])
        if x:
            return x.group()
        else:
            return ''
    except IndexError:
        return ''
        
ctrl_c_pressed = False
def ctrl_c_handler(signal, frame):
    global ctrl_c_pressed
    ctrl_c_pressed = True

# Eigene Funktion zur Behandlung des Keyboard-Interrupts (Ctrl+C) anmelden
signal.signal(signal.SIGINT, ctrl_c_handler)

# ===================================================================

sock = init_socket()
init_prologix()
init_keithley2700(KEITHLEY2700_ADR)

while True:
    gpib_write('READ?')
    data = gpib_read()
    if data:
        print('Wert: >>>{}<<<'.format(extract_value(data)))
    if ctrl_c_pressed: # erst unterbrechen, wenn Kommunikation abgeschlossen
        break
    time.sleep(.5)

sock.close()
print('\nEnde!')
sys.exit(0)
    
