# Prologix-GPIB-ETHERNET-Controller

## Allgemeine Hinweise

* [Home-Page der Firma »Prologix«](http://prologix.biz/)

Die Netzwerk-Konfiguration des GPIB-ETHERNET-Controllers kann sinngemäß folgendermaßen erfolgen:

```
python ./nfcli.py --ip_type=static --ip_addr="192.168.98.57" \
--netmask="255.255.255.0" --gateway="192.168.98.1" --eth_addr="00-21-69-01-23-E6"
```

Das Skript ist [hier](nfcli/dist/script/) oder in [diesem
tar-Archiv](http://prologix.biz/downloads/nfcli.tar.gz) zu finden.

## Anwendungsbeispiele

Zu Testzwecken lässt sich der GPIB-ETHERNET-Controller per »telnet« ansprechen (»1234« ist die feste Port-Adresse des Controllers):

```
telnet 172.30.54.71 1234
Trying 172.30.54.71...
Connected to 172.30.54.71.
Escape character is '^]'.
++auto 0
++addr 22                              # GPIB-Geräteadresse festlegen
*IDN?                                  # Identifier des Gerätes abfragen
++read eoi                             # Antwort abholen
HEWLETT-PACKARD,34401A,0,4-1-1
MEAS:VOLT:DC?                          # Gleichspannungsmessung auslösen
++read eoi                             # Antwort abholen
+8.88698400E+00
```

Die folgenden Python-Scripte

* [`keithley2700-test.py`](./keithley2700-test.py)
* [`hp34401a-test.py`](./hp34401a-test.py)
* [`combi-test.py`](./combi-test.py) `# Auslesen zweier Geräte` 

zeigen die Kommunikation mit dem Adapter in einfachen Beispielen. 
