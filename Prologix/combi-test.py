#!/usr/bin/env python3

# Rolf Niepraschk, Rolf.Niepraschk@ptb.de, 2022-11-29

import socket, signal, sys, re, time

PROLOGIX_IP = '172.30.54.71'
PROLOGIX_PORT = 1234
SOCKET_TIMEOUT = 10   # s
DEVICE_TIMEOUT = 3000 # ms (max. 3000)
HP34401A_ADR = 22
KEITHLEY2700_ADR = 6
TX_EOL = b'\n' # Endemarkierung beim Senden

BUFSIZE = 256

sock = None

def init_socket():
    try:    
        x = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
        x.settimeout(SOCKET_TIMEOUT)
        x.connect((PROLOGIX_IP, PROLOGIX_PORT))
        time.sleep(.1)
    except socket.error as e:
        print ('Error creating socket: %s' % e)  
        sys.exit(1) 
    return x

def socket_read():
    return sock.recv(BUFSIZE).decode('ascii').strip()

def gpib_write(s, adr=None):
    try:
        if adr:
            set_gpib_addr(adr)
        return sock.send(s.encode('ascii') + TX_EOL)
    except socket.error as e:
        print ('Error write to socket: %s' % e)
        return 0
    
def gpib_read(adr):
    try:
        gpib_write('++read eoi', adr)
        return socket_read()
    except socket.error as e:
        print ('Error read from socket: %s' % e)
        return ''

current_device_address = 0
def set_gpib_addr(adr):
    global current_device_address
    if adr != current_device_address:
        gpib_write('++addr ' + str(adr))
        current_device_address = adr
    
def init_prologix():
    gpib_write('++savecfg 0')# Don't write configuration to eprom (!)
    gpib_write('++mode 1')# Set mode as controller
    #gpib_write('++ifc')# Controller-In-Charge # nötig?
    gpib_write('++auto 0')# Turn off read-after-write
    gpib_write('++ver')
    print('='*72)
    print(socket_read())
    print('='*72)
    gpib_write('++read_tmo_ms ' + str(DEVICE_TIMEOUT))
    
def init_voltage():
    # Initialisierung des HP 34401A
    adr = HP34401A_ADR
    set_gpib_addr(adr)
    gpib_write('++clr', adr)
    gpib_write('++eoi 1', adr)
    gpib_write('*IDN?', adr)
    print(gpib_read(adr))
    print('='*72)
    gpib_write('TRIG:COUN 1', adr)
    gpib_write('TRIG:SOUR IMM', adr)
    # Messgröße, Messbereich und Auflösung anpassen
    gpib_write('CONF:VOLT:DC', adr)
    time.sleep(.1)# nötig?

def init_temperature():
    # Initialisierung des KEITHLEY 2700
    adr = KEITHLEY2700_ADR
    set_gpib_addr(adr)
    gpib_write('++clr', adr)
    gpib_write('++eoi 1', adr)
    gpib_write('*IDN?', adr)
    print(gpib_read(adr))
    print('='*72)
    ###gpib_write('INIT:CONT OFF', adr)# Continuous measurement mode off
    gpib_write('TRIG:COUN 1', adr)
    gpib_write('TRIG:SOUR IMM', adr)
    # Messgröße, Messbereich und Auflösung anpassen
    gpib_write('CONF:VOLT:DC', adr)
    time.sleep(.1)# nötig?

extract_pattern = re.compile(r'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?')        
def extract_value(d):
    '''
    -3.43784839E-02VDC,+727.217SECS,+00148RDNG#
    '''
    try:
        a = d.split(',')
        x = extract_pattern.search(a[0])
        if x:
            return x.group()
        else:
            return ''
    except IndexError:
        return ''

def read_temperature():
    gpib_write('READ?', KEITHLEY2700_ADR)
    data = gpib_read(KEITHLEY2700_ADR)
    if data:
        return extract_value(data)
    else:
        return ''
    
def read_voltage():
    gpib_write('READ?', HP34401A_ADR)
    data = gpib_read(HP34401A_ADR)
    if data:
        return data
    else:
        return ''
        
ctrl_c_pressed = False
def ctrl_c_handler(signal, frame):
    global ctrl_c_pressed
    ctrl_c_pressed = True

# Eigene Funktion zur Behandlung des Keyboard-Interrupts (Ctrl+C) anmelden
signal.signal(signal.SIGINT, ctrl_c_handler)
            
# ===================================================================
    
sock = init_socket()
init_prologix()
init_voltage()
init_temperature()

while True:
    print('{}: >>>{}<<<'.format('Spannung', read_voltage()))
    print('{}: >>>{}<<<'.format('Temperatur', read_temperature()))
    print()
    if ctrl_c_pressed: # erst unterbrechen, wenn Kommunikation abgeschlossen
        break
    #time.sleep(.5)

sock.close()
print('\nEnde!')
sys.exit(0)



