# Python-Beispiele

Die Unterverzeichnisse sind für Beispiele von Python-Code, der sich leicht
wiederverwenden lässt, gedacht. Anregungen können aus der Art der
Programmierung wie auch der Art, Messgeräte u.ä. anzusprechen, geschöpft
werden. Der Code erhebt nicht den Anspruch, besonders ausgefeilt zu sein. Man
sollte daher keine Scheu haben, selbst etwas hinzuzufügen.

Ein kostenloses e-Book zu »Python«:

* Johannes Ernesti, Peter Kaiser: [Python 3 –
Das umfassende Handbuch](https://openbook.rheinwerk-verlag.de/python/index.html)

