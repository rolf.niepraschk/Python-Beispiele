#!/usr/bin/env python3

# Rolf Niepraschk, Rolf.Niepraschk@ptb.de

import subprocess

TEX_COMPILER = 'lualatex'
RUNS = 2
infile = 'sample2e.tex' # Diese Datei findet TeX immer ohne weiteres.

def latex_compile(filename, num=1, compiler='pdflatex'):
    i = num
    r = -1
    while i > 0:
        output = subprocess.run([compiler, '--interaction=nonstopmode', \
          filename], stdout=subprocess.DEVNULL)
        r = output.returncode
        if r != 0:
            break
        i -= 1
    return True if r == 0 else False

result = latex_compile(infile, RUNS, TEX_COMPILER)
if result:
    print('\n*** erfolgreich! ***')
else:
    print('\n*** fehlerhaft! ***')# In .log-Datei nachsehen...




