#!/usr/bin/env python3

# Rolf Niepraschk, Rolf.Niepraschk@ptb.de, 2023-03-21

import sys, requests

LAN_RS232_HOST = 'e75491'
LAN_RS232_PORT = 10001
LAN_RS232_TIMEOUT = 10000 # ms

CORVUS_ETX = '\r'
CORVUS_GETPOS = ' pos{ETX}'.format(ETX=CORVUS_ETX)

DEVHUB_HOST = 'i75419' # 'localhost'
DEVHUB_PORT = 9009
DEVHUB_URL = 'http://{}:{}/'.format(DEVHUB_HOST, DEVHUB_PORT)

"""
Zum PostProcessing (JAVASCRIPT): Parameter ist ein String-Array. Jeder String
wird als JavaScript-Code interpretiert. Über die spezielle Variable "_" hat
man Zugriff auf vordefinierte Funktionen, wie in diesem Beispiel
"extractCorvusArray". Ihre Definition findet man in der Datei
"resources/js/add-CORVUS.js".

Die folgende Abfrage gibt einen ungefähren Überblick, an welchen Stellen der
Datenbankdokumente "PostProcessing" verwendet wird:

curl -s a73434:5984/vl_db/_all_docs?include_docs=true | jq | grep -6 PostProcessing
(Achtung: Dauert lange.)

Die Schlüsselworte "PostScript" (für Closure-Code) und "PostScriptPy"
(für Python-Code) haben eine ähnliche Funktion wie "PostProcessing".

""" 
print()
print('='*78)

print('\n*** JSON-Auftrag an DEVHUB ***')
print('*** Postprocessing per JAVASCRIPT-Code ***')
devhub_data = {
  'Action':'TCP', 'Host':LAN_RS232_HOST, 'Port':LAN_RS232_PORT,
  'Timeout':LAN_RS232_TIMEOUT, 'Value': CORVUS_GETPOS,
  'PostProcessing': [
    'var Result = _.extractCorvusArray(_x)[1];'
  ]
}
print(devhub_data)

response = requests.post(DEVHUB_URL, json=devhub_data)
data = response.json() # erzeugt aus JSON eine Dictionary-Variable

#print(response.status_code) # http-Status; sollte "200" sein

print('\n*** JSON-Antwort von DEVHUB ***')
print(data)
# enthält die Angaben des JSON-Auftrags ergänzt um die Antwortdaten wie
# "_x", "_t_start", "_t_stop", "req-id" und ggf. "Result" aus dem Postprocessing.

print('\n*** Rohdaten der Geräteantwort ***')
print(data.get('_x'))

print('\n*** Ergebnis des Postprocessings (Position Motor 2, Index 1) ***')
print(data.get('Result'))

print('='*78)

print('\n*** JSON-Auftrag an DEVHUB ***')
print('*** Postprocessing per Python-Code ***')

devhub_data = {
  'Action':'TCP', 'Host':LAN_RS232_HOST, 'Port':LAN_RS232_PORT,
  'Timeout':LAN_RS232_TIMEOUT, 'Value': CORVUS_GETPOS,
  'PostScriptPy': 'corvus', 'PostScriptInput': {'Index':1}
}
print(devhub_data)

response = requests.post(DEVHUB_URL, json=devhub_data)
data = response.json()

print('\n*** JSON-Antwort von DEVHUB ***')
print(data)

print('\n*** Rohdaten der Geräteantwort ***')
print(data.get('_x'))

print('\n*** Ergebnis des Postprocessings (Position Motor 2, Index 1) ***')
print(data.get('Result'))

print('='*78)

print('\nEnde!')

sys.exit(0)

