#!/usr/bin/env python3

# Rolf Niepraschk, Rolf.Niepraschk@ptb.de, 2023-03-09

import sys, requests
import json # debug!

DEVHUB_HOST = 'i75419' # 'localhost'
DEVHUB_PORT = 9009
DEVHUB_URL = 'http://{}:{}/'.format(DEVHUB_HOST, DEVHUB_PORT)

LAN_RS232_HOST = 'e75491'
LAN_RS232_TIMEOUT = 10000 # ms
DATA_TEMPLATE = { 'Action':'TCP', 'Host':LAN_RS232_HOST }

CORVUS_ETX = '\r'
CORVUS_GETPOS = ' pos{ETX}'.format(ETX=CORVUS_ETX)
#CORVUS_MOVEREL = 'clean\r1.2 sv\r0.0 0.0 -8 r\r1 getpitch\r'
CORVUS_MOVEREL = 'clean{ETX}{VEL} sv{ETX}0 {R1} {R2} r{ETX}0.0 0.0 0.0 r{ETX}'

VALVE = { 
  'grLW':          { 'Port':10001, 'Index':0, 'Velocity':1.2 },
  'klLW':          { 'Port':10001, 'Index':1, 'Velocity':1.2 },
  'DVG1':          { 'Port':10003, 'Index':0, 'Velocity':1.2 },
  'Verdränger':    { 'Port':10003, 'Index':1, 'Velocity':0.8 }
}

PROMPT = 'J/N ? '

def _find_getch():
# https://stackoverflow.com/questions/510357/how-to-read-a-single-character-from-the-user
    try:
        import termios
    except ImportError:
        # Non-POSIX. Return msvcrt's (Windows') getch.
        import msvcrt
        return msvcrt.getch

    # POSIX system. Create and return a getch that manipulates the tty.
    import sys, tty
    def _getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

    return _getch

getch = _find_getch()

def y_n_input(prompt, true_chars):
    print (prompt, end='', flush=True)
    x = getch()
    if ord(x) == 3: # "Strg C"
        print('\nEnde!')
        sys.exit(1)
    return x in true_chars

def is_movement_needed(n, pos):
    x = False
    p = float(pos)
    print('Position »{}«: {}mm'.format(n, pos))
    if p != 0:
        corr = -p
        print('Soll »{}« um {}mm zur Nullposition gefahren werden?'
          .format(n, corr))
        x = y_n_input(PROMPT, ['j','J'])
    else:
        print('»{}« ist bereits auf Nullposition. Es ist nichts zu tun.'
          .format(n))
    print()
    return x

def call_devhub(data):
    response = requests.post(DEVHUB_URL, json=data)
    return response.json()    

def get_pos(n):
    port = VALVE[n]['Port']
    idx = VALVE[n]['Index']
    d = DATA_TEMPLATE.copy()
    d['Timeout'] = LAN_RS232_TIMEOUT
    d['Port'] = port
    d['Value'] = CORVUS_GETPOS
    print('==> get_pos: ' + json.dumps(d))
    res = call_devhub(d)
    print('==> _x: {}'.format(res['_x']))
    a = res['_x'].split()
    print('==> result: ' + a[idx])
    #pos = a[idx]
    pos = 8 # dummy
    return pos
    
def move_rel(n, dist):
    port = VALVE[n]['Port']
    idx = VALVE[n]['Index']
    vel = VALVE[n]['Velocity']
    d = DATA_TEMPLATE.copy()
    d['Port'] = port
    r1 = 0
    r2 = 0
    if idx == 0:
        r1 = dist
    elif idx == 1:
        r2 = dist
    d['Value'] = CORVUS_MOVEREL.format(ETX=CORVUS_ETX, VEL=vel, R1=r1, R2=r2)
    # res = call_devhub(d)
    print('==> move: ' + json.dumps(d))
    ###pos = get_pos(n)
    pos = 0 # dummy
    return pos

# ----------------------------------------------------------------------------

print('\n')

###print(json.dumps(DATA_TEMPLATE))

print('='*64)
for name in VALVE:
    pos = get_pos(name)
    mov = is_movement_needed(name, pos)
    if mov:
        corr = -pos
        print('\n{} wird um {}mm bewegt ...'.format(name, corr))
        res = move_rel(name, corr)
        print('Resultat: {}mm'.format(res))    
    print('='*64)

print('\nEnde!')

sys.exit(0)


