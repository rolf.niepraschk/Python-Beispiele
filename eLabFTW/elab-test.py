#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""
@author: Rolf Niepraschk
"""

import datetime
import elabapi_python

#########################
#         CONFIG        #
#########################
API_HOST_URL = 'https://elabftw.ptb.de/api/v2'
API_KEY = 'a2c6b77fdd5b19b35b3043dbfd5ac73883c4265ab210a3672037ace64a6207cfa4231a0a834ed30c19be8'
###       ^^^--- Utes Token ...
#########################
#      END CONFIG       #
#########################

# Configure the api client
configuration = elabapi_python.Configuration()
configuration.api_key['api_key'] = API_KEY
configuration.api_key_prefix['api_key'] = 'Authorization'
configuration.host = API_HOST_URL
configuration.debug = False
configuration.verify_ssl = False

# create an instance of the API class
api_client = elabapi_python.ApiClient(configuration)
# fix issue with Authorization header not being properly set by the generated lib
api_client.set_default_header(header_name='Authorization', header_value=API_KEY)

# create an instance of Experiments
exp_api = elabapi_python.ExperimentsApi(api_client)

experiment_nb = 344

exp = exp_api.get_experiment(experiment_nb)
print('='*72)
# === der bisherige Zustand === 
print('\n ******** Experiment {} (Original): \n'.format(experiment_nb), exp)
print('='*72)

now = datetime.datetime.now()
new_body='<p>Neu von Rolf ({})</p>'.format(now.strftime('%Y-%m-%d %H:%M:%S'))
data = { 
  'body': new_body
}
# === Modifizieren der Einträge (hier Eintrag "body") ===
response = exp_api.patch_experiment(experiment_nb, body=data)
print('='*72)
# === der modifizierte Zustand ===
print('\n******** Experiment {} (modifiziert): \n'.format(experiment_nb), response)
print('='*72)





