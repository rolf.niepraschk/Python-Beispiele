# Test zum Zugriff auf »eLabFTW« via APIv2

Siehe dazu die [API-Dokumetation](https://doc.elabftw.net/api/elabapi-html/) von »eLabFTW«

## Anlegen der virtuellen Umgebung und Installation der eLab-Bibliothek

```
python3 -m venv .
source bin/activate
pip3 install elabapi_python

```

**Anmerkung:** Nur einmalig notwendig, wenn die Python-Bibliothek `elabapi_python` nicht bereits installiert ist.

## Aktivieren der virtuellen Umgebung

```
source bin/activate
```

**Anmerkung:** Nur notwendig, wenn die Python-Bibliothek `elabapi_python` nicht bereits installiert ist.

## Test des Modifizierens eines Experiments

```
python3 elab-test.py
```

RN, 2020-07-05
