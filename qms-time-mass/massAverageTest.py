#!/usr/bin/env python3

# Rolf.Niepraschk@ptb.de, 2022-08-25 

import sys
from collections import OrderedDict

def getMassAverage(filename, tbegin = None, tend = None, trange = None):
    def error_exit(x):
        print('ERROR: ', str(x), file=sys.stderr)
        sys.exit(1)
    if not trange and (not tbegin or not tend):
        error_exit('Missing time range')
    try:
        d = {}
        with open(filename, 'r') as f:
            line = f.readline()
            dataValid = False
            while line:
                if dataValid:
                    # Ende der Daten
                    if line.startswith('"[Annotations/User Records'):
                        dataValid = False
                    else:
                        l = line.strip()
                        a = l.split('\t')# An Tabulatoren trennen
                        # a[0] = "Time", a[1] = "Scan", a[2] = "Mass 4", a[3] = "Sum Scanned Masses"
                        if a[0]:
                            key = a[0].strip('"')
                            # Timestamp als key für Dictionary
                            d[key] = a[2]
                else:
                    # Beginn der Daten
                    if line.startswith('"Time"'):
                        dataValid = True
                line = f.readline()            
    except:
        error_exit("Can't read file: " +  fname)
    finally:
        f.close()
    # Sortieren bei neueren Python-Versionen unnötig?    
    data = OrderedDict(sorted(d.items()))
    
    def doAverage(beg, end):
        sum = 0.0
        cnt = 0
        for key in data:
            if key >= beg and key < end:
              #print(key, '-->', data[key])
              cnt += 1
              sum += float(data[key])
        return sum / cnt
    
    tb = []
    te = []
    res = []
    
    if trange:
        if not isinstance(trange,list):
            trange = [trange]# Einzelwert zu Array machen
        for x in trange:
            a = x.split(' -')
            tb.append(a[0].strip())
            te.append(a[1].strip())
    else:
        if not isinstance(tbegin,list):
            tb = [tbegin]# Einzelwert zu Array machen
        if not isinstance(tend,list):
            te = [tend]# Einzelwert zu Array machen
    
    i = 0
    while i < len(tb):
        s = doAverage(tb[i], te[i])
        res.append(s)
        i += 1
    if len(res) == 1:
        return res[0]# Bei nur einem Wert: kein Array
    else:
        return res
      
x = getMassAverage(filename='QMS75152.txt',trange='01.08.2022 15:10:08 -01.08.2022 15:12:07 ')

print('Mass Avarage: ', x)
print('=====================================')

trangeArray = [
  "01.08.2022 14:18:58 -01.08.2022 14:20:58 ",
  "01.08.2022 14:42:56 -01.08.2022 14:44:55 ",
  "01.08.2022 15:10:08 -01.08.2022 15:12:07 "
]

x = getMassAverage(filename='QMS75152.txt',trange=trangeArray)
i = 0
while i < len(x):
    print(i, 'Mass Avarage: ', x[i])
    i += 1
   
