#!/usr/bin/env python3

# Rolf Niepraschk, Rolf.Niepraschk@ptb.de

import sys, serial, time
from datetime import datetime

VERSION='2022-11-14' 

# Konfigurationsdaten für serielle Schnittstelle
PORT='/dev/ttyUSB0'
BAUDRATE=9600
BYTESIZE=serial.EIGHTBITS
PARITY=serial.PARITY_NONE
STOPBITS=serial.STOPBITS_ONE
TIMEOUT=5

TX_EOL = b'\n'
AXIS='1'
FILENAME='m230-calibration.csv'
MIN_POS=0  #  ???
MAX_POS=55 # ???

SEC_PER_MM = .1 

def write(s):
    try:
        return ser.write(s.encode('ascii') + TX_EOL)
    except serial.SerialException:
        return 0
        
def read():
    try:
        d = ser.readline()
        return d.decode('ascii').strip()
    except (serial.SerialException, UnicodeDecodeError):
        return '?' 
            
def write_read(s):
    write(s)
    return read()
    
def init_serial_port():
    try:    
        x = serial.Serial(PORT, baudrate=BAUDRATE, bytesize=BYTESIZE, \
        parity=PARITY, stopbits=STOPBITS, timeout=TIMEOUT, xonxoff=False, \
        rtscts=False, dsrdtr=False)
        print(str(x).replace(' ', '\n'))
    except (serial.SerialException, ValueError) as e:
        print('Could not open serial device {}: {}'.format(PORT, e))
        exit(1)
    if not x.is_open:
        x.open()
    x.reset_input_buffer()
    x.reset_output_buffer()
    return x

def setup_mercury():
    print('\nInitialisierung »Mercury«')
    print('-------------------------')
    d = write_read('*IDN?')
    print('Device Identification: {}'.format(d))
    d = write_read('VER?')
    print('Firmware Version: {}'.format(d))
    d = write_read('CSV?')
    print('Current Syntax Version: {}'.format(d))
    # TODO: Was ist davon wichtig?
    """
    write('SVO 1 1')    #  Set Servo Mode
    write('RON 1 0')    #  Set Reference Mode
    write('POS 1 0')    #  Set Real Position
    write('DFH 1')      #  Define Home Position
    write('GOH 1')      #  Go To Home Position 
    write('FNL 1')      #  Fast Reference Move To Negative Limit
    write('FPL 1')      #  Fast Reference Move To Positive Limit
    write('MOV 1 X')    #  Set Target Position
    write('MOV? 1')     #  Get Target Position
    write('POS 1 X')    #  Set Real Position
    write('POS? 1')     #  Get Real Position
    write('RBT')        #  Reboot System
    """    

def check_numeric(x):
    try:
        val = float(x)
        return True
    except ValueError:
        return False
    
def num_input(prompt, exit_string=None):
    ok = False
    p = prompt
    if exit_string:
      p = '(Ende = ' + exit_string + ') ' + prompt
    while not ok:
        x = input(p).replace(',', '.')# Komma oder Punkt als Dezimaltrenner gestatten
        ok = check_numeric(x)
        if ok:
            return float(x)
        else:
            if x == exit_string:
                return x
            else:
                print('    Fehler: Eingabe muss eine Zahl sein!')

def get_date():
    now = datetime.now()
    return now.strftime('%Y-%m-%d %H:%M:%S') 

def get_pos():
    """ # TODO: Echte Position erfragen
    pos = write_read('???')
    return pos
    """
    return 17.4
    
def move_to(p):
    """ # TODO: Zu Position p fahren
    write('???')
    """

print('\nAG 7.54: Kommunikation mit Motorsteuerung »Mercury«')
print('===================================================')
print('(Version: {})'.format(VERSION))
    
ser = init_serial_port()
setup_mercury()
print('\n')

delta = num_input('Schrittweite in mm: ')
print('\n')
_pos = 0.0

with open(FILENAME, 'w') as f:
    f.write('# Kalibrierung M-230 (AG 7.54, ' + get_date() + ')\n')
    f.write('# =================================================\n')
    f.write('# Sollwert, Istwert\n')
    f.write('#\n') # Header-Daten in Datei schreiben
    while True:
        move_to(_pos) # Bewegen des Motors zur nächsten Vorgabeposition
        time.sleep(delta * SEC_PER_MM) # Warten bis Position erreicht ist
        pos = get_pos() # Erreichte reale Position abfragen
        # Vergleichsposition eingeben
        x = num_input('\n    Sollwert: {}    Istwert: '.format(pos), 'exit')
        if not isinstance(x, float): # 'exit' wurde eingegeben
            break
        else:
            f.write('{}, {}\n'.format(pos, x)) # Daten in Datei schreiben
        _pos += delta # nächste Position 

print('\n*** ENDE ***\n')

ser.close()


